package propins.web;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.After;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.BaseTest;

import java.io.File;
import java.io.IOException;

public class Login extends BaseTest {

  public String url = "https://test.propins.cl/login";
  private WebElement emailInput;
  private WebElement passwordInput;
  private WebElement clickElement;
  private WebElement loginName;
  private WebElement popupText;

  @Given("URL login section")
  public void url_login_section() {
    driver.get(url);
  }

  @When("I fill email {string}")
  public void i_fill_email(String email) {
    emailInput = driver.findElement(By.name("email"));
    emailInput.sendKeys(email);
  }

  @And("I fill password {string}")
  public void i_fill_password(String password) {
    passwordInput = driver.findElement(By.name("password"));
    passwordInput.sendKeys(password);
  }

  @And("I click on \"Iniciar Sesión\" button")
  public void i_click_button_login() {
    clickElement = driver.findElement(By.cssSelector("#login > form > div.row > div:nth-child(1) > button"));
    clickElement.click();
  }

  @Then("user is logged and show the {string}")
  public void user_is_logged(String name) throws InterruptedException, IOException {
      WebDriverWait wait = new WebDriverWait(driver, 10);
      wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#basic-nav-dropdown")));
      loginName = driver.findElement(By.cssSelector("#basic-nav-dropdown"));
      Assert.assertEquals(loginName.getText(), name);
  }

  @Then("I see a popup with \"Error al iniciar sesión\" message")
  public void i_see_a_popup_message() throws Exception {
    WebDriverWait wait = new WebDriverWait(driver, 10);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body > div.swal-overlay.swal-overlay--show-modal > div")));
    popupText = driver.findElement(By.cssSelector("body > div.swal-overlay.swal-overlay--show-modal > div > div.swal-title"));
    Assert.assertEquals(popupText.getText(), "Error al iniciar sesión");
   // this.takeSnapShot(driver, "/Users/jesideyrincon/propins/evidence/errormessage.png");
  }

  @After
  public void closeDriver() {
    driver.close();
  }

}

