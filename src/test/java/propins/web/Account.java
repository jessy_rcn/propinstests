package propins.web;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.BaseTest;
import propins.web.Login;

public class Account extends BaseTest {

    public String url = "https://test.propins.cl/login";
    private WebElement clickElement;
    private WebElement namesInput;
    private WebElement lastnamesInput;
    private WebElement rutInput;
    private WebElement emailInput;
    private WebElement phonenumberInput;
    private WebElement passwordInput;
    private WebElement confirmpasswordInput;
    private WebElement checkelement;

    @Given("URL in login section")
    public void url_login_section() {
        driver.get(url);
    }

    @When("I click on \"Crear Cuenta\" button")
    public void i_click_button_account() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#login > form > div.row > div:nth-child(2) > button")));
        clickElement = driver.findElement(By.cssSelector("#login > form > div.row > div:nth-child(2) > button"));
        clickElement.click();
    }

    @And("I fill names field {string}")
    public void i_fill_names_account(String names){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("nombres")));
        namesInput = driver.findElement(By.name("nombres"));
        namesInput.sendKeys(names);
    }

    @And("I fill last names field {string}")
    public void i_fill_lastnames_account(String lastnames){
        lastnamesInput = driver.findElement(By.name("apellidos"));
        lastnamesInput.sendKeys(lastnames);
    }

    @And("I fill rut field {string}")
    public void i_fill_rut_account(String rut){
        rutInput = driver.findElement(By.name("rut"));
        rutInput.sendKeys(rut);
    }

    @And("I fill an email {string}")
    public void i_fill_email(String email) {
        emailInput = driver.findElement(By.name("mail"));
        emailInput.sendKeys(email);
    }

    @And("I fill phone number field {string}")
    public void i_fill_phonenumber_account(String phonenumber){
        phonenumberInput = driver.findElement(By.name("telefono"));
        phonenumberInput.sendKeys(phonenumber);
    }

    @And("I fill a password {string}")
    public void i_fill_password(String password) {
        passwordInput = driver.findElement(By.name("password"));
        passwordInput.sendKeys(password);
    }

    @And("I fill confirm password {string}")
    public void i_fill_confirm_password(String passwordc) {
        confirmpasswordInput = driver.findElement(By.name("passwordConfirm"));
        confirmpasswordInput.sendKeys(passwordc);
    }

    @And("I check on \"Aceptar Términos y Condiciones\" checkbox")
    public void i_check_tc(){
        checkelement = driver.findElement(By.id("terminos"));
        checkelement.click();
    }

    @And("I click on \"Registrar Ahora\" button")
    public void i_click_register_account() {
        clickElement = driver.findElement(By.cssSelector("#login > form > div.row > div > button"));
        clickElement.click();
    }

    @Then("account is created")
    public void account_created() {
    }

    @After
    public void closeDriver() {
        driver.close();
    }
}
