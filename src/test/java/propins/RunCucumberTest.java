package propins;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(glue = {"propins"},
        tags = {"@loginErrors, @loginSuccessful, @LoginServiceSuccesfull"})
public class RunCucumberTest {
}
