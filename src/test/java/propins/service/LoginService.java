package propins.service;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.specification.RequestSpecification;
import io.restassured.response.Response;
import model.LoginPojo;
import org.junit.Assert;

import static io.restassured.RestAssured.given;

public class LoginService {

    LoginPojo login = new LoginPojo();
    private RequestSpecification request;
    private Response response;

    @Given("I have data to login an user with {string}, {string}")
    public void i_have_data_to_login_an_user_with(String email, String password) {
        login.setEmail(email);
        login.setPassword(password);
    }

    @Given("I use login header")
    public void i_use_login_header() {
        request = given().header("Content-Type", "application/json").header("Accept-Charset", "UTF-8")
                .log()
                .headers();
    }

    @When("I create post request to login an WebUser")
    public void i_create_post_request_to_login_an_webuser() {
        response = request.when()
                .body(login)
                .post("http://23.96.1.55/api/Usuario/LoginUsuarioWeb");
    }

    @Then("I get status code 200 from database")
    public void i_get_status_code_from_database() {
        Assert.assertEquals(200, response.getStatusCode());
    }

    @And("I should save the token in a variable")
    public void i_should_save_the_token_in_a_variable(){
        String token = response.jsonPath().getString("value.data.token");
    }


}
