package util;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.io.FileHandler;

import java.io.File;

import static org.junit.Assert.*;

public class BaseTest {
  public WebDriver driver = new FirefoxDriver();

  public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws Exception {
    //Convert web driver object to TakeScreenshot
    TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
    //Call getScreenshotAs method to create image file
    File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
    //Move image file to new destination
    File DestFile = new File(fileWithPath);
    //Copy file at destination
    FileHandler.copy(SrcFile, DestFile);
  }

}
