@login
Feature: Login in application

  @loginSuccessful
  Scenario Outline:Login successful
    Given URL login section
      When I fill email "<Email>"
      And I fill password "<Password>"
      And I click on "Iniciar Sesión" button
      Then user is logged and show the "<Name>"

    Examples:
      | Email                  | Password   |  Name              |
      | jesidey@mailinator.com | 123123123  |  Jesi Rincón Mora  |

  @loginErrors
  Scenario Outline:Login with an invalid password and User without an account
    Given URL login section
    When I fill email "<Email>"
    And I fill password "<Password>"
    And I click on "Iniciar Sesión" button
    Then I see a popup with "Error al iniciar sesión" message

    Examples:
      | Email                  | Password |
      | jesidey@mailinator.com | hola124  |
      | jesid@mailinator.com   | hola124  |
