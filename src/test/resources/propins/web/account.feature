@account
  Feature: Account section in aplication

    @createAccountSuccessful
    Scenario Outline:Create a successful account
      Given URL in login section
      When I click on "Crear Cuenta" button
      And I fill names field "<Names>"
      And I fill last names field "<LastNames>"
      And I fill rut field "<Rut>"
      And I fill an email "<Email>"
      And I fill phone number field "<PhoneNumber>"
      And I fill a password "<Password>"
      And I fill confirm password "<ConfirmPassword>"
      And I check on "Aceptar Términos y Condiciones" checkbox
      And I click on "Registrar Ahora" button
 #     Then account is created

      Examples:
        | Email                     | Password   |  Names          | LastNames | Rut         | PhoneNumber | ConfirmPassword |
        | jesidey_1@mailinator.com  | 123123123  |  Jesi Cristina  | Mora Mora | 11111111-1  | 952087654   | 123123123       |