Feature: Login in Service

  Scenario Outline: Login Succesfull with a Service
    Given I have data to login an user with "<Email>", "<Password>"
    And I use login header
    When I create post request to login an WebUser
    Then I get status code 200 from database
    And I should save the token in a variable

    Examples:
    | Email                   | Password  |
    | jesidey@mailinator.com  | 123123123 |